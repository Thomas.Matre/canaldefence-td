package inf101v22.towerDefence;

import inf101v22.towerDefence.view.GameView;

import javax.swing.*;

public class Main {

    static JFrame frame = new JFrame();
    static GameView canvas = new GameView();

    public static void main(String[] args) {

        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(canvas);
        frame.setVisible(true);
    }
}
