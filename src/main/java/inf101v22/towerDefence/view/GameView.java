package inf101v22.towerDefence.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class GameView extends JPanel implements ActionListener {

    Timer timer;

    Ellipse2D.Double ball = new Ellipse2D.Double(10, 10, 50, 50);
    Rectangle2D.Double collision = new Rectangle2D.Double(100, 100, 100, 100);

    public GameView () {
        this.setPreferredSize(new Dimension(500, 500));
        this.setBackground(Color.BLACK);

        timer = new Timer(10, this);
        timer.start();


    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2D = (Graphics2D) g;

        g2D.setColor(Color.orange);
        g2D.fill(ball);

        g2D.fill(collision);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        ball.x ++;
        ball.y ++;

        if (collision.intersects(ball.getBounds2D())) {
            System.out.println("HIT");
        }

        repaint();
    }
}
